<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?>> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title><?php bloginfo( 'name' ); wp_title('|'); ?></title>
    <meta name="description" content="<?php bloginfo( 'description' ); ?>">
	
	<?php wp_head(); ?>

</head>
<body <?php body_class(); ?> >

<div class="top-bar">
    <div class="navigation-search">
            
        <?php

            get_template_part( 'partials/contact_info' );

         ?>

    </div>
</div>

<div class="logo-navigation-bar">

    <div class="logo-navigation">

        <?php 

            get_template_part('partials/logo');

            get_template_part( 'partials/navigation' );

        ?>

        <span class="hamburger-menu-button"></span>

    </div>
    
</div>