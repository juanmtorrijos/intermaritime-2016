<?php

/**
 * Register styles and scripts the WordPress way
 */
function register_my_styles_and_scripts() {
	global $post;
	
	wp_register_style( 'main_style', THEMEROOT . '/css/main.css', array(), '40', 'all' );
	wp_enqueue_style( 'main_style' );

	if ( is_page_template( 'home_page.php' ) ) {
		wp_enqueue_script( 'lightbox_js', THEMEROOT . '/js/video-lightbox.js', array('jquery'), '2016', true );
		wp_enqueue_script('numberAnimate', THEMEROOT . '/js/min/animate-number-min.js', array('jquery'), '32', true);
	}

	if ( is_page_template( 'about_us-page.php' ) ) {
		wp_enqueue_script('numberAnimate', THEMEROOT . '/js/min/animate-number-min.js', array('jquery'), '32', true);	
	}

	if ( is_page_template('contact_us-page.php') ) {
		
	}

	if ( is_page_template( 'login-page.php' ) ) {
		wp_enqueue_script( 'ajax_login_js', THEMEROOT . '/js/ajax-login.js', array('jquery'), '2016', true );
		wp_localize_script( 'ajax_login_js', 'itt_login_obj', array(
			'ajaxurl'		=> admin_url( 'admin-ajax.php' ),
			'security'		=> wp_create_nonce( 'itt_login_nonce' ),
			'redirecturl'	=> 'login-area'
		));
	}

	if ( has_shortcode( $post->post_content, 'vessel-registration-calculator' ) ) {
		wp_enqueue_script('vessel-registration-calculator', THEMEROOT . '/js/vessel-registration-calculator.js', array('jquery'), '4', true);
		wp_localize_script('vessel-registration-calculator', 'vrcalculator_obj', array(
			'redirect_url' => get_permalink( 16359 )
		));
	}

	if ( function_exists( 'wpcf7_enqueue_scripts' ) ) {
        wpcf7_enqueue_scripts();
    }
 
    if ( function_exists( 'wpcf7_enqueue_styles' ) ) {
        wpcf7_enqueue_styles();
    }

	wp_enqueue_script( 'main_js', THEMEROOT . '/js/mobile-menu.js', array('jquery'), '2016', true );

}

add_action( 'wp_enqueue_scripts', 'register_my_styles_and_scripts' );


/**
 * Loading JavaScript and stylesheet only when it is necessary
 */

add_filter( 'wpcf7_load_js', '__return_false' );
add_filter( 'wpcf7_load_css', '__return_false' );

/**
 * Remove Autop from contact form 7
 */

add_filter('wpcf7_autop_or_not', '__return_false');
