<?php

//[current-year]
function current_year( $atts ){
	return date('Y');
}
add_shortcode( 'current-year', 'current_year' );

function the_user( $atts ) {
	$the_user = wp_get_current_user();
	return $the_user->display_name;
}

add_shortcode( 'user', 'the_user' );

function logout_href( $atts, $content ) {
	$redirect = home_url( 'login' );
	return '<a href="' . wp_logout_url( $redirect ) . '">' . $content . '</a>';
}

add_shortcode( 'logout', 'logout_href' );