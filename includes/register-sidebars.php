<?php 
/**
 * Register sidebar
 * 
 * @return string
 *
 */
function intermaritime_sidebar(){
	/**
	* Creates a sidebar
	* @param string|array  Builds Sidebar based off of 'name' and 'id' values.
	*/
	$sidebar_args = array(
		'name'          => 'Sidebar for Pages',
		'id'            => 'sidebar_menu',
		'description'   => '',
		'class'         => '',
		'before_widget' => '<div class="sidebar">',
		'after_widget'  => '</div>',
		'before_title'	=> '<h3>',
		'after_title'	=> '</h3>'
	);

	register_sidebar( $sidebar_args );

	$footer_args = array(
		'name' 			=> 'Footer Menu',
		'id'			=> 'footer-menu',
		'class'         => 'footer-menu',
		'before_widget'	=> '<div class="footer-widget">',
		'after_widget'	=> '</div>',
	);

	register_sidebar( $footer_args );
}

add_action( 'widgets_init', 'intermaritime_sidebar' );

