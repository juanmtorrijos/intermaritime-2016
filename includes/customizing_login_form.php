<?php

function intermaritime_login_logo() {
	?>
		<link rel="stylesheet" href="<?php echo THEMEROOT; ?>/css/login.css?ver=2">
	<?php
}

add_action( 'login_enqueue_scripts', 'intermaritime_login_logo' );

function icsclass_login_url() {
	return home_url();
}

add_filter( 'login_headerurl', 'icsclass_login_url' );

function icsclass_login_url_title() {
	return get_bloginfo( 'name' );
}

add_filter( 'login_headertitle', 'icsclass_login_url_title' );
