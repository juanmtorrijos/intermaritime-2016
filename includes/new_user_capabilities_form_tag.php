<?php

function itmt_add_user_capability_new_form_tag() {
?>
<table class="form-table">
    <tr>
		<th scope="row"><?php _e( 'User Capabilities' ) ?></th>
		<td><label for="itmt_classification"><input type="checkbox" name="itmt_classification" id="itmt_classification" value="1" checked  /> <?php _e( 'Classification' ); ?></label></td>
		<td><label for="itmt_load_lines"><input type="checkbox" name="itmt_load_lines" id="itmt_load_lines" value="1" checked /> <?php _e( 'Load Lines' ); ?></label></td>
	</tr>
	<tr>
		<th></th>
		<td><label for="itmt_solas"><input type="checkbox" name="itmt_solas" id="itmt_solas" value="1" checked /> <?php _e( 'SOLAS' ); ?></label></td>
		<td><label for="itmt_marpol"><input type="checkbox" name="itmt_marpol" id="itmt_marpol" value="1" checked /> <?php _e( 'MARPOL' ); ?></label></td>
	</tr>
	<tr>
		<th></th>
		<td><label for="itmt_tonnage"><input type="checkbox" name="itmt_tonnage" id="itmt_tonnage" value="1" checked /> <?php _e( 'Tonnage' ); ?></label></td>
		<td><label for="itmt_chemical_gas_tankers"><input type="checkbox" name="itmt_chemical_gas_tankers" id="itmt_chemical_gas_tankers" value="1" checked /> <?php _e( 'Chemical Tankers and Gas Carriers' ); ?></label></td>
	</tr>
	<tr>
		<th></th>
		<td><label for="itmt_mody_code"><input type="checkbox" name="itmt_mody_code" id="itmt_mody_code" value="1" checked /> <?php _e( 'MODU - Code' ); ?></label></td>
		<td><label for="itmt_ism_code"><input type="checkbox" name="itmt_ism_code" id="itmt_ism_code" value="1" checked /> <?php _e( 'ISM - Code' ); ?></label></td>
	</tr>
	<tr>
		<th></th>
		<td><label for="itmt_isps_code"><input type="checkbox" name="itmt_isps_code" id="itmt_isps_code" value="1" checked /> <?php _e( 'ISPS - Code' ); ?></label></td>
		<td><label for="itmt_u_500"><input type="checkbox" name="itmt_u_500" id="itmt_u_500" value="1" checked /> <?php _e( 'Vessels Under 500 GT' ); ?></label></td>
	</tr>
	<tr>
		<th></th>
		<td><label for="itmt_caribbean"><input type="checkbox" name="itmt_caribbean" id="itmt_caribbean" value="1" checked /> <?php _e( 'Caribbean Code' ); ?></label></td>
		<td><label for="itmt_fishing_vessels"><input type="checkbox" name="itmt_fishing_vessels" id="itmt_fishing_vessels" value="1" checked /> <?php _e( 'Fishing Vessels' ); ?></label></td>
	</tr>
	<tr>
		<th></th>
		<td><label for="itmt_ilo"><input type="checkbox" name="itmt_ilo" id="itmt_ilo" value="1" checked /> <?php _e( 'ILO Convention' ); ?></label></td>
		<td><label for="itmt_other_certs"><input type="checkbox" name="itmt_other_certs" id="itmt_other_certs" value="1" checked /> <?php _e( 'Other Certificates' ); ?></label></td>
	</tr>
</table>
<?php
}

add_action( 'user_new_form', 'itmt_add_user_capability_new_form_tag');