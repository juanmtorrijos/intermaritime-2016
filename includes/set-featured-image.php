<?php


add_theme_support( 'post-thumbnails', array( 'page' ) );

add_filter( 'admin_post_thumbnail_html', 'add_featured_image_instruction');

function add_featured_image_instruction( $content ) {
    return $content .= '<p>This image will appear as the background for the page title. It will not appear in small screens. The image must be at least 1920 x 100 px size.</p>';
}
