<?php

function intermaritime_loginout_menu_link( $items, $args ) {
	if ($args->theme_location == 'top_bar_menu') {
		if (is_user_logged_in()) {
			$items .= '<li><a href="'. wp_logout_url( home_url() ) .'">'. __("Log Out") .'</a></li>';
			$items .= '<li><a href="'. wp_login_url('login-area') .'">'. __("Login Area") .'</a></li>';
		} else {
			$items .= '<li><a href="'. wp_login_url( home_url() ) .'">'. __("Log In") .'</a></li>';
		}
	}
	return $items;
}

add_filter( 'wp_nav_menu_items', 'intermaritime_loginout_menu_link', 10, 2 );

function intermaritime_login_area_menu_links( $items, $args ) {
	if ($args->theme_location == 'login_area_menu') {
		if (is_user_logged_in()) {
			$items .= '<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-logout"><a href="'. wp_logout_url( home_url('login') ) .'"><i class="_mi _before dashicons dashicons-migrate" aria-hidden="true"></i><span>Log Out</span></a></li>';
		}
	}
	return $items;
}

add_filter( 'wp_nav_menu_items', 'intermaritime_login_area_menu_links', 10, 2 );