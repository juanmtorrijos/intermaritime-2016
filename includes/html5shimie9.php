<?php

/**
 * Prints HTML 5 Shim for old IE Browsers
 * 
 */
function html_5_shim_ie9() {
   echo '<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->';
}
add_action( 'wp_head', 'html_5_shim_ie9' );