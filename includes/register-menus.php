<?php
register_nav_menus( array(
	'main_menu' 		=> __('Main Menu', 'intermaritime_theme'),
	'footer_menu'		=> __('Footer Menu', 'intermaritime_theme'),
	'top_bar_menu' 		=> __('Top Bar Menu', 'intermaritime_theme'),
	'login_area_menu' 	=> __('Login Area Menu', 'intermaritime_theme'),
	'sidebar_menu'	 	=> __('Sidebar Menu', 'intermaritime_theme'),
));