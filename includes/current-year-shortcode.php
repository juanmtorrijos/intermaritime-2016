<?php

//[current-year]
function current_year( $atts ){
	return date('Y');
}
add_shortcode( 'current-year', 'current_year' );