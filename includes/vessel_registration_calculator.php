<?php

function vessel_registration_calculator() {

	ob_start();

	?>
		<form class="vessel-calculator">
			<div class="type-of-vessel">
				<p class="in-form">Type of Vessel:</p>
				<div class="vessel-type-row radios">
					<label>
						<input type="radio" name="type-of-vessel" value="Passenger Vessel" checked>
						<p>Passenger Ships</p>
					</label>
					
					<label>
						<input type="radio" name="type-of-vessel" value="Oil Tanker">
						<p>Oil Tanker</p>
					</label>

					<label>
						<input type="radio" name="type-of-vessel" value="Cargo Vessel">
						<p>Cargo Vessel</p>
					</label>
					
					<label>
						<input type="radio" name="type-of-vessel" value="Drilling Vessel">
						<p>Drilling Vessel</p>
					</label>
					
					<label>
						<input type="radio" name="type-of-vessel" value="Mobile Offshore Drilling Units (MODU)">
						<p>Mobile Offshore Drilling Units (MODU)</p>
					</label>

					<label>
						<input type="radio" name="type-of-vessel" value="Non Self Propelled">
						<p>Non Self Propelled Vessel</p>
					</label>
					
					<label>
						<input type="radio" name="type-of-vessel" value="Scientific Exploration">
						<p>Any vessel not related to commercial or profit activities</p>
					</label>
					
					<label>
						<input type="radio" name="type-of-vessel" value="Yacht">
						<p>Yacht</p>
					</label>
					
					<label>
						<input type="radio" name="type-of-vessel" value="Other">
						<p>Other</p>
					</label>
				</div>
			</div>
			<div class="yachts radios">
				<p class="in-form">Is your Yacht for:</p>
				<label>
					<input type="radio" name="type-of-yacht" value="Commercial" checked>
					<p>Commercial Use</p>
				</label>
				<label>
					<input type="radio" name="type-of-yacht" value="Personal">
					<p>Personal Use</p>
				</label>
				<p class="in-form">Is the owner's nationality:</p>
				<label>
					<input type="radio" name="owner-nationality" value="Panamanian">
					<p>Panamanian</p>
				</label>
				<label>
					<input type="radio" name="owner-nationality" value="Other Nationality" checked>
					<p>Other Nationality</p>
				</label>
			</div>
			<div class="vessel-details">
				<div class="tonnage show-div">
					<label>
						<p class="in-form">Gross Tonnage <small>only integers (no decimals)</small></p>
						<input type="number" id="gross-tonnage">
					</label>
					<label>
						<p class="in-form">Net Tonnage <small>only integers (no decimals)</small></p>
						<input type="number" id="net-tonnage">
					</label>	
				</div>
				<label>
					<p class="in-form">Name of Onwer/Company <small>if available</small></p>
					<input type="text" id="name-of-owner">
				</label>
				<label>
					<p class="in-form">Name of Vessel <small>if available</small></p>
					<input type="text" id="name-of-vessel">
				</label>
			</div>
			<div class="calculate">
				<button type="submit">Calculate</button>
			</div>
		</form>
		<div class="application">
			<div class="inner-application">
				<h4>Vessel Registration Government Fee:</h4>
				<div class="yacht-registration-rate">
					<p></p>
				</div>
				<table class="vessel-registration-government-fees">
					<thead>
						<tr>
							<th>Section</th>
							<th>Description</th>
							<th>Amount</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>I</td>
							<td>Flag Registration Rights Fee</td>
							<td id="flag-registration-rights"></td>
						</tr>
						<tr>
							<td>II</td>
							<td>Annual Taxes</td>
							<td id="annual-taxes"></td>
						</tr>
						<tr>
							<td>III</td>
							<td>Annual Consular Rate</td>
							<td id="annual-consular-rate"></td>
						</tr>
						<tr>
							<td>IV</td>
							<td>Annual Inspection Fee</td>
							<td id="annual-inspection"></td>
						</tr>
						<tr>
							<td>V</td>
							<td>Tariff for Investigations, Conferences and International Treaties</td>
							<td id="tariff-for-investigations"></td>
						</tr>
						<tr>
							<td>VI</td>
							<td>Per Net Tonnage</td>
							<td id="per-net-tonnage"></td>
						</tr>
					</tbody>
					<tfoot>
						<tr>
							<td colspan="2">* Estimated Panama Maritime Authority Fee 🇵🇦</td>
							<td id="vessel-fee"></td>
						</tr>
					</tfoot>
				</table>
				<div class="note">
					<p>* This fee is approximate and does not include legal fees such as registration and documentation services. </p>
					<p> Please contact us for more information using the form bellow. <em>Thank you for your interest in our services.</em></p>
					<?php echo do_shortcode('[contact-form-7 id="17049" title="Vessel Registration Calculator"]'); ?>
				</div>
			</div>
		</div>
	<?php

	$calculator_form = ob_get_clean();

	return $calculator_form;

}

add_shortcode( 'vessel-registration-calculator', 'vessel_registration_calculator' );
