	<footer>
		<div class="footer">
			<div class="logobox">
				<img src="<?php echo IMAGESPATH; ?>/intermaritime-logo-footer.png" alt="">
			</div>
		</div>
	</footer>

	<div class="copyright">
		<p>&copy; <strong>Copyright</strong> <span class="intermaritime-name">InterMaritime</span> 2005 - <?php echo date( 'Y'); ?>. All rights reserved.</p>
	</div>

		<?php wp_footer(); ?>
	</body>

</html>