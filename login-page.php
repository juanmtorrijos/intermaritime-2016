<?php 

/**
 * Template Name: Login Page
 */

if ( !is_user_logged_in() ) {

	get_header('login');

	get_template_part( 'partials/page', 'login_form' );

	get_footer('login');

} else {

	wp_redirect( home_url( 'login-area' ) );

}

?>