	<footer>
		<div class="footer">
			<div class="logobox">
				<img src="<?php echo IMAGESPATH; ?>/intermaritime-logo-footer.png" alt="">
				<p>Intermaritime is a company based in Panama City Panama, with offices and partners around the world, dedicated to provide bespoke maritime solutions.</p>
				<ul class="social">
					<li class="instagram"><a href="https://www.instagram.com/intermaritime/"></a></li>
					<li class="linkedin"><a href="https://www.linkedin.com/company/intermaritime-certification-services-ics-class-"></a></li>
					<li class="twitter"><a href="https://twitter.com/ITMaritime"></a></li>
					<!-- <li class="youtube"><a href="https://www.youtube.com/watch?v=wxAWXqqDNLQ"></a></li> -->
				</ul>
			</div>
			<div class="footerbox">
				<h3>Our Services</h3>
				<?php 
					/**
	                * Displays a navigation menu
	                * @param array $args Arguments
	                */
	                $footer_args = array(
	                    'theme_location'    => 'footer_menu',
	                    'container'         => '',
	                    'container_class'   => 'footer-nav',
	                    'menu_class'        => 'footer-nav',
	                    'echo'              => true,
	                    'fallback_cb'       => 'wp_page_menu',
	                );
	            
	                wp_nav_menu( $footer_args );

				 ?>
			</div>
			<div class="footerbox">
				<h3>Get in Touch</h3>
				<p>77th Street, San Francisco, Panama City, Rep. of Panama</p>
				<p>info@intermaritime.org</p>
				<p>+(507) 322-0013</p>
			</div>
			<div class="footerbox">
				<h3>Join Our Newsletter</h3>
				<p>Stay connected with latest maritime news</p>
				<?php get_template_part('partials/mailchimp_form'); ?>
			</div>
		</div>
	</footer>

	<div class="copyright">
		<p>&copy; <strong>Copyright</strong> <span class="intermaritime-name">InterMaritime</span> 2005 - <?php echo date( 'Y'); ?>. All rights reserved.</p>
	</div>
	
		<?php wp_footer(); ?>
	</body>

</html>