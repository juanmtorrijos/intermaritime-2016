<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?>> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title><?php wp_title(); ?></title>
        <meta name="description" content="<?php bloginfo( 'description' ); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		
		<?php wp_head(); ?>

    </head>
    <body >

    <?php get_template_part( 'partials/corporate-video' ); ?>

    <?php get_template_part( 'partials/contact-modal' ); ?>

    <div class="top-bar">
        <div class="logo-navigation">
            
            <div class="logo">
                <a href="<?php echo home_url(); ?>">
                    <img src="<?php echo THEMEROOT; ?>/images/intermaritime-logo.png" alt="<?php bloginfo( 'description' ); ?>">
                </a>
            </div>

            <ul class="main-nav">
                <li><a class="class-bg" href="http://icsclass.org">Class</a></li>
                <li><a class="legal-bg" href="http://pml.com.pa">Legal</a></li>
                <li><a class="training-bg" href="http://panamamaritimetraining.com">Training</a></li>
                <li><a class="surveys-bg" href="http://icsurveys.org">Surveys</a></li>
                <li><a class="insurance-bg" href="http://aqmltd.com">Insurance</a></li>
            </ul>

        </div>
    </div>

    <?php get_template_part( 'partials/banner', 'home' ); ?>

    <?php get_template_part( 'partials/icons', 'home' ); ?>

    <?php get_template_part( 'partials/about', 'home' ); ?>

    <?php get_template_part( 'partials/work-with-us', 'home' ); ?>

    <?php wp_footer(); ?>

    </body>

</html>