; (function( $ ) {
$(function() {
var $loginForm = $('form#login');

$loginForm.on('submit', function(e){

    var $loader = $('img.loader');
    var $pStatus = $('p.status');
    var $username = $('input#username');
    var $password = $('input#password');
    var $security = $('input#security');

    $pStatus.show().text(ajax_login_object.loadingmessage);

    $loader.show();

    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: ajax_login_object.ajaxurl,
        data: { 
            'action': 'ajaxlogin', //calls wp_ajax_nopriv_ajaxlogin
            'username': $username.val(),
            'password': $password.val(),
            'security': $security.val(),
        },
        success: function(data){
            $('form#login p.status').text(data.message);
            if (data.loggedin === true){
                document.location.href = ajax_login_object.redirecturl;
            }
        }
    });
    e.preventDefault();
});

});
})(jQuery); 	