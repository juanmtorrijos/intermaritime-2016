var itt_login_obj;

jQuery.noConflict();
(function( $ ) {
$(function() {

var statusMessage = $('.status-message'),
	loginLoading = $('.login-loading-animation'),
	loginForm = $('form.login-form'),
	loginButton = loginForm.find('input#wp-submit'),
	userLogin = $('input#user_login'),
	userPass = $('input#user_pass'),
	shakeIt = 'animated shake',
	redBorder = {
					'border'	: '1px solid #C10000',
					'background': '#E34250',
					'color'		: 'white'
				};

loginForm.on('submit', function(event){

	event.preventDefault();

	loginLoading.fadeIn(450);

	$.ajax({
		type : 'POST',
		dataType : 'json',
		url : itt_login_obj.ajaxurl,
		data : {
			'action' : 'ajaxlogin',
			'security' : itt_login_obj.security,
			'username' : userLogin.val().toString(),
			'password' : userPass.val().toString()
		},
		success : function(data){
			if ( data.loggedin === false ) {
				statusMessage
					.text(data.message)
					.css(redBorder)
					.show()
					.addClass(shakeIt);
				loginLoading.fadeOut(300);
				loginButton.val('Try Again');
			} else {
				document.location.href = itt_login_obj.redirecturl;
			}
		},
		error : function(){
			statusMessage
			.text('Sorry an error ocurred. Please try again later.')
			.css(redBorder)
			.show();
			loginLoading.fadeOut(450);
			return;
		}
	});
});

});
})(jQuery);