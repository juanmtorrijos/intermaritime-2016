; (function( $ ) {
$(function() {

var $window = $(window);
var $vessels = $('#vessels');
var $countries = $('#countries');
var vesselsTop = $vessels.position().top;

function animateVCNumbers(){

	if ( $window.scrollTop() > ( vesselsTop - $window.height() ) ) {
		$window.off('scroll', animateVCNumbers);
		$vessels.animateNumber({
			number: 5238,
			easing: 'easeOut',
		}, 1000);
		$countries.animateNumber({
			number: 83,
			easing: 'easeOut',
		}, 1500);
	}
}

$window.on('scroll', animateVCNumbers);

});
})(jQuery);