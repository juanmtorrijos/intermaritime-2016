var vrcalculator_obj;

(function( $ ) {
$(function() {

var vesselTypeInput = $('input[name="type-of-vessel"]'),
	yachtTypeInput = $('input[name="type-of-yacht"]'),
	yachtDiv = $('.yachts'),
	tonnageDiv = $('.tonnage'),
	vesselCalculatorForm = $('form.vessel-calculator'),
	calculateButton = $('div.calculate > button'),
	applicationDiv = $('.application'),
	nameOfOwner = $('input#name-of-owner'),
	companyName = $('input#company-name'),
	nameOfVessel = $('input#name-of-vessel'),
	yachtRegistartionRate = $('.yacht-registration-rate'),
	vesselRegistrationTable = $('.vessel-registration-government-fees'),
	flagRegistrationTable = $('#flag-registration-rights'),
	annualTaxesTable = $('#annual-taxes'),
	annualConsularRateTable = $('#annual-consular-rate'),
	annualInspectionFeeTable = $('#annual-inspection'),
	tariffForInvTable = $('#tariff-for-investigations'),
	perNetTonnTable = $('#per-net-tonnage'),
	vesselFee = $('#vessel-fee'),
	registrationRate,
	/**
	 * Hidden Fields declarations
	 */
	mailjetVesselType = $('#type-of-vessel'),
	mailjetYachtType = $('#type-of-yacht'),
	mailjetOwnerNationality = $('#owner-nationality'),
	mailjetGrossTonnage = $('#gross-tonnage-mailjet'),
	mailjetNetTonnage = $('#net-tonnage-mailjet'),
	mailjetNameOfVessel = $('#name-of-vessel-mailjet'),
	mailjetFlagRegistrationRights = $('#flag-registration-rights-mailjet'),
	mailjetAnnualTaxes = $('#anual-taxes-mailjet'),
	mailjetAnnualConsularRate = $('#annual-consular-rate-mailjet'),
	mailjetInspectionFee = $('#annual-inspection-mailjet'),
	mailjetTariffForInv = $('#tariff-for-investigation-mailjet'),
	mailjetRegistrationFee = $('#registration-fee-mailjet');

function toggleTonnageDiv () {
	if ($(this).val() === 'Personal') {
		tonnageDiv.removeClass('show-div');
	} else {
		if (tonnageDiv.not('show-div')) {
			tonnageDiv.addClass('show-div');
		}
	}
}

function toggleYachtsDiv () {
	if ($(this).val() === 'Yacht') {
		yachtDiv.addClass('show-div');
	} else {
		if (yachtDiv.hasClass('show-div')) {
			yachtDiv.removeClass('show-div');
			if (tonnageDiv.not('show-div')) {
				tonnageDiv.addClass('show-div');
			}
		}
	}
}

function numberWithCommas(number) {
	return Number(number).toLocaleString('en', {style: 'currency', currency: 'USD', minimumFractionDigits: 2});
}

function getRegistrationRate(event){
	event.preventDefault();

	var vesselType = $('input[name="type-of-vessel"]:checked')[0].value,
		yachtType = $('input[name="type-of-yacht"]:checked')[0].value,
		yachtOwnerNationality = $('input[name="owner-nationality"]:checked')[0].value,
		grossTonnage = ($('#gross-tonnage').val() === '') ? 0 : parseInt($('#gross-tonnage').val()),
		netTonnage = ($('#net-tonnage').val() === '') ? 0 : parseInt($('#net-tonnage').val());
	
	var	flagRegistrationRights,
		annualTaxes,
		annualConsularRate,
		annualInspection,
		tariffForInvestigation,
		perNetTonnage;
	
	/**
	 * Flag Registration Rights P.I
	 */
	if (grossTonnage <= 2000) {
		flagRegistrationRights = 500;
	} else if ( grossTonnage > 2000 && grossTonnage <= 5000 ) {
		flagRegistrationRights = 2000;
	} else if ( grossTonnage > 5000 && grossTonnage <= 15000 ) {
		flagRegistrationRights = 3000;
	} else {
		flagRegistrationRights = ((3000 + (grossTonnage * 0.01)) < 6500) ? (3000 + (grossTonnage * 0.01)) : 6500;
	}

	/**
	 * Anual Taxes P.II
	 */
	annualTaxes = (netTonnage * 0.10);

	/**
	 * Annual Consular Rate P.III
	 */
	if (vesselType === 'Non Self Propelled') {
		if (grossTonnage <= 500) {
			annualConsularRate = 850;
		} else if (grossTonnage > 500 && grossTonnage <= 1000) {
			annualConsularRate = 1400;
		} else if (grossTonnage > 1000) {
			annualConsularRate = 1800;
		}
	} else {
		if (grossTonnage <= 1000) {
			annualConsularRate = 1200;
		} else if (grossTonnage > 1000 && grossTonnage <=3000) {
			annualConsularRate = 1800;
		} else if (grossTonnage > 3000 && grossTonnage <= 5000) {
			annualConsularRate = 2000;
		} else if (grossTonnage > 5000 && grossTonnage <= 15000) {
			annualConsularRate = 2700;
		} else if (grossTonnage > 15000) {
			annualConsularRate = 3000;
		}
	}

	/**
	 * Annual Inspection P.IV
	 */
	if (vesselType === 'Passenger Vessel' || vesselType === 'Yacht') {
		if (grossTonnage <= 1600) {
			annualInspection = 900;
		} else if (grossTonnage > 1600) {
			annualInspection = 1800;
		}
	} else if (vesselType === 'Oil Tanker' || vesselType === 'Cargo Vessel') {
		if (grossTonnage <= 500) {
			annualInspection = 500;
		} else if (grossTonnage > 500 && grossTonnage <= 1600) {
			annualInspection = 750;
		} else if (grossTonnage > 1600 && grossTonnage <= 5000) {
			annualInspection = 850;
		} else if (grossTonnage > 5000 && grossTonnage <= 15000) {
			annualInspection = 1000;
		} else if (grossTonnage > 15000) {
			annualInspection = 1200;
		}
	} else if (vesselType === 'Drilling Vessel') {
		annualInspection = 1300;
	} else {
		if (grossTonnage <= 500) {
			annualInspection = 500;
		} else if (grossTonnage > 500 && grossTonnage <= 5000) {
			annualInspection = 800;
		} else if (grossTonnage > 5000) {
			annualInspection = 1000;
		}
	}

	/**
	 * Tariff for Investigations, Conferences and International Treaties P.V
	 */
	if (vesselType === 'Oil Tanker' || vesselType === 'Passenger Vessel') {
		tariffForInvestigation = 850;
	} else {
		if (grossTonnage <= 500) {
			tariffForInvestigation = 300;
		} else if (grossTonnage > 500 && grossTonnage <= 10000) {
			tariffForInvestigation = 400;
		} else if (grossTonnage > 10000) {
			tariffForInvestigation = 500;
		}
	}

	/**
	 * Per Net Tonnage P.VI
	 */
	perNetTonnage = (netTonnage * 0.03);

	/**
	 * Total Registration Rate for PMA
	 */
	if (vesselType === 'Yacht' && yachtType === 'Personal' && yachtOwnerNationality === 'Panamanian') {
		registrationRate = 1000;
	}
	else if (vesselType === 'Yacht' && yachtType === 'Personal' && yachtOwnerNationality === 'Other Nationality') {
		registrationRate = 1500;
	}
	else {
		registrationRate = flagRegistrationRights + annualTaxes + annualConsularRate + annualInspection + tariffForInvestigation + perNetTonnage;
	}

	if (vesselType === 'Yacht' && yachtType === 'Personal') {
		vesselRegistrationTable.hide();
		yachtRegistartionRate.show();
		yachtRegistartionRate.find('p').html('* Estimated Panama Maritime Authority Fee 🇵🇦: ' + numberWithCommas(registrationRate));
	}

	calculateButton.html('Calculating...').attr('disabled', 'disabled');
	
	setTimeout( function(){
		flagRegistrationTable.html(numberWithCommas(flagRegistrationRights));
		annualTaxesTable.html(numberWithCommas(annualTaxes));
		annualConsularRateTable.html(numberWithCommas(annualConsularRate));
		annualInspectionFeeTable.html(numberWithCommas(annualInspection));
		tariffForInvTable.html(numberWithCommas(tariffForInvestigation));
		perNetTonnTable.html(numberWithCommas(perNetTonnage));
		applicationDiv.addClass('show-div');
		vesselFee.html(numberWithCommas(registrationRate));
		companyName.val( nameOfOwner.val() );
		calculateButton.html('Try Again').removeAttr('disabled');
		$('html, body').animate({
			scrollTop: (applicationDiv.offset().top - 80)
		}, 800);
	}, 925);

	/**
	 * Fill Hidden Fields for Sending
	 */
	mailjetVesselType.val(vesselType);
	mailjetYachtType.val(yachtType);
	mailjetOwnerNationality.val(yachtOwnerNationality);
	mailjetGrossTonnage.val(grossTonnage);
	mailjetNetTonnage.val(netTonnage);
	mailjetNameOfVessel.val(nameOfVessel.val());
	mailjetFlagRegistrationRights.val(numberWithCommas(flagRegistrationRights));
	mailjetAnnualTaxes.val(numberWithCommas(annualTaxes));
	mailjetAnnualConsularRate.val(numberWithCommas(annualConsularRate));
	mailjetInspectionFee.val(numberWithCommas(annualInspection));
	mailjetTariffForInv.val(numberWithCommas(tariffForInvestigation));
	mailjetRegistrationFee.val(numberWithCommas(registrationRate));
}

vesselTypeInput.on('click', toggleYachtsDiv);

yachtTypeInput.on('click', toggleTonnageDiv);

vesselCalculatorForm.on('submit', getRegistrationRate);

document.addEventListener( 'wpcf7mailsent', function() {
	var redirecUrl = location;
    location = vrcalculator_obj.redirect_url;
}, false );

});
})(jQuery);