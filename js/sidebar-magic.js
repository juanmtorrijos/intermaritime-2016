(function( $ ) {
$(function() {

	var $window = $(window),
		windowWidth = $window.width(),
		$sidebar = $('.sidebar'),
		$innerSidebar = $('.menu-sidebar-menu-container'),
		sidebarTop = $sidebar.offset().top,
		sidebarHeight = $innerSidebar.height(),
		sidebarHeadingHeight = $sidebar.find('h3')[0].offsetHeight,
		$mainContainer = $('.main-content'),
		mainContainerWidth = $mainContainer.width(),
		mainContainerMargin = ($window.width() / 2) - (mainContainerWidth / 2),
		$footer = $('.footer'),
		$vesselCalculatorForm = $('form.vessel-calculator');
		
		function getWindowTop(){
			var windowScrollTop = $window.scrollTop(),
			footerTop = $footer.offset().top,
			footerStop = (footerTop - (sidebarHeight + sidebarHeadingHeight + 72));

		if ( windowScrollTop < sidebarTop ) {
			$sidebar.removeClass('sticky');
		} else if ( windowScrollTop >= sidebarTop && windowScrollTop < footerStop ) {
			$sidebar.addClass('sticky');
			$sidebar.removeClass('stuck');
			$sidebar.css('right', (mainContainerMargin + 'px'));
		} else if ( windowScrollTop >= footerStop) {
			$sidebar.removeClass('sticky');
			$sidebar.addClass('stuck');
			$sidebar.css({
				'bottom': 0,
				'right': '20px'
			});
		}
	}

	function setSidebarHeight(){		
		setTimeout( function(){
			var footerTop = $footer.offset().top;
		}, 926);
	}

	if ( windowWidth > 700 ) {
		$window.on('scroll', getWindowTop);
		$vesselCalculatorForm.on('submit', setSidebarHeight);
	}

});
})(jQuery);  