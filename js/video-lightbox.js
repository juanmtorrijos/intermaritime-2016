; (function( $ ) {
$(function() {

var $corporateVideo = $('.corporate-video'),
    $theVideo = $('.the-video'),
    $window = $(window),
    $videoBtnDiv = $('.video'),
    $playVideoBtn = $('a.play-video'),
    $contactModal = $('.contact-modal'),
    $modalForm = $('.modal-form'),

    $contactUsBtn = $('a.contact-us');

var theVideoPlayer = document.getElementById('intermaritime-corporate-video');

function fadeTheVideoIn() {
  var theVideoPos = $window.scrollTop();
  $corporateVideo.fadeIn();
  $theVideo.fadeIn();
  $theVideo.css({
    'margin-top' : theVideoPos + ($window.height() / 2) - ($theVideo.height() / 2)
  });
  theVideoPlayer.play();
}

function fadeTheVideoOut(){
  $corporateVideo.fadeOut();
  $theVideo.fadeOut();
  theVideoPlayer.pause();
}

function fadeMessageIn(){
  $videoBtnDiv.hide();
}

function fadeModalFormIn(){
  var theModalPos = $window.scrollTop();
  $contactModal.fadeIn('fast');
  $modalForm.fadeIn('slow');
  $modalForm.css({
    'margin-top' : theModalPos + ($window.height() / 2) - ($modalForm.height() / 2)
  });
}

function fadeModalFormOut(){
  $modalForm.fadeOut('fast');
  $contactModal.fadeOut('slow');
  $modalForm.find('form').find("input[type=text], textarea").val("");
}

$playVideoBtn.on('click', fadeTheVideoIn);

$corporateVideo.on('click', fadeTheVideoOut).children().click(function(){
  return false;
});

theVideoPlayer.addEventListener('ended', function(){
  fadeTheVideoOut();
  fadeMessageIn();
});

$contactUsBtn.on('click', fadeModalFormIn);

$contactModal.on('click', fadeModalFormOut).children().click(function(){
  return false;
});

});
})(jQuery);