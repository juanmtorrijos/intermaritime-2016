jQuery.noConflict();
(function( $ ) {
$(function() {

var hamburgerMenuBtn = $('span.hamburger-menu-button'),
	navigation = $('.main-nav');

function toggleMenu(){
	navigation.slideToggle();	
}

hamburgerMenuBtn.on('click', toggleMenu);

});
})(jQuery);