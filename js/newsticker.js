; (function( $ ) {
$(function() {

var $newsText 		= $('.news-text'),
	$windowWidth 	= $(window).width();

function newsTicker() {

	$newsText.find('li').addClass('slide-up').first().appendTo($newsText).end().removeClass('slide-up');

}

if ( $windowWidth > 940) {
	setInterval( function(){
		newsTicker();
	}, 4000 );
}

});
})(jQuery);