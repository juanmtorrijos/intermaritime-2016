<?php

/**
 * Template Name: Contact Us Page
 */

get_header();

get_template_part('partials/about_page', 'title');

get_template_part( 'partials/contact_page', 'content' );

get_footer();