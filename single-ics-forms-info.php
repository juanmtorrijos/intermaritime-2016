<?php 

if ( is_user_logged_in() ) {

	get_header('login_area');

	get_template_part('partials/page', 'title_login_area');

	get_template_part('partials/page_single', 'main_content');

	get_footer('login_area');

} else {

	wp_redirect( home_url( 'login_area' ) );
	
}

?>