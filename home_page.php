<?php 

/**
 * Template Name: Home Page 
 */

get_header();

get_template_part( 'partials/corporate-video' );

get_template_part( 'partials/banner', 'home' );

get_template_part( 'partials/latest_news', 'home' );

get_template_part( 'partials/icons', 'home' );

get_template_part( 'partials/about', 'home' );

get_template_part( 'partials/work-with-us', 'home' );

get_footer(); ?>