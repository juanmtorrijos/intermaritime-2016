<?php 

/**
 * Template Name: About Us Page
 * 
 */

get_header();

get_template_part('partials/about_page', 'title');

get_template_part('partials/about_page', 'main_content');

get_template_part( 'partials/about', 'stats' );

get_template_part( 'partials/work-with-us', 'home' );

get_footer();