<?php 

/**
 * Template Name: Services Page
 */

get_header();

get_template_part( 'partials/services', 'navigation' );

get_template_part( 'partials/services', 'main_content' );

get_footer();

?>