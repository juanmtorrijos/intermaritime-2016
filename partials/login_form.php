<div class="login_form">
	<div class="status-message"></div>
	<div class="login-loading-animation"></div>
	<form class="login-form" action="login">

		<label>Username</label>
		<input type="text" name="user_login" id="user_login" class="input" />

		<label>Password</label>
		<input type="password" name="user_pass" id="user_pass" class="input" />

		<div class="submit-button-rememberme">
			<label><input name="pmts_rememberme" id="pmts_rememberme" type="checkbox" checked="checked" class="rememberme" value="forever" />Remember Me</label>

			<input type="submit" name="wp-submit" id="wp-submit" value="Log In" />
			<input type="hidden" name="login-with-ajax" value="login" />
		</div>

	</form>
</div>