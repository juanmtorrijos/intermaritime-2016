<div class="main-content">

	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

		<p><?php the_content(); ?></p>

	<?php endwhile; else : ?>

		<p>No posts...</p>

	<?php endif; wp_reset_query(); ?>

</div>