<div class="about-home">
	<div class="about-text-stats">
		<div class="about-text">
			<p>
				<span class="intermaritime-letters">InterMaritime</span> is your one stop shop for complete maritime solutions, tailor made to your needs.
				We cover Class and Statutory Certification, Seaferer Training, Legal Counselling, Independent Surveys and Insurance.
				<a href="<?php echo home_url( 'about-us' );; ?>">Learn more about us &raquo;</a>
			</p>
		</div>

		<div class="stats">
			<div class="vesels">
				<p>
					<span class="number" id="vessels">50,000</span>
					<span class="text">Vessels Covered</span>
				</p>
			</div>

			<div class="countries">
				<p>
					<span class="number" id="countries">80</span>
					<span class="text">Countries Served</span>
				</p>
			</div>
		</div>
	</div>
</div>