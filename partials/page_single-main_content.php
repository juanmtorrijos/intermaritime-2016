<div class="main-content">

	<?php if ( have_posts() ) : while ( have_posts() ) : the_post();?>

		<h1><?php the_title(); ?></h1>

		<p><?php the_content(); ?></p>

	<?php endwhile; else : ?>

		<p>No posts...</p>

	<?php endif; wp_reset_query(); ?>

	<?php 

		$ics_forms_args = array(
			'post_type' => 'ics-forms-info',
			'post_parent' => $post->ID,
			'posts_per_page' => -1,
		);

		$ics_forms_children = new WP_Query( $ics_forms_args );

		if ( $ics_forms_children->have_posts() ) : 

	?>

	<div class="ics-forms">
			

		<ul>

			<?php while ( $ics_forms_children->have_posts() ) : $ics_forms_children->the_post(); ?>

				<li>
					<a href="<?php echo the_permalink(); ?>">
						<?php the_title(); ?>
					</a>
				</li>

			<?php endwhile; ?>

		</ul>

	</div>

	<?php endif; ?>

</div>
