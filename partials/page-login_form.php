<div class="login-form-content">

	<div class="intermaritime-login-logo">
		<a href="<?php echo home_url(); ?>">		
			<img src="<?php echo IMAGESPATH; ?>/intermaritime-login-logo.png" alt="">
		</a>
		
		<a href="<?php echo home_url(); ?>">&laquo; Back to InterMaritime Website</a>
		
	</div>

	<div class="login-content">

	<?php 
		//The Title
		if ( have_posts() ) : while ( have_posts() ) : the_post(); 
		the_title('<h1>', '</h1>' ); 
		endwhile; endif; wp_reset_query(); 
		
		//The Login Form
		get_template_part('partials/login_form');

	?>
	

	</div>

	<div class="login-content">
		
		<?php	
			//The Text
			if ( have_posts() ) : while ( have_posts() ) : the_post(); 
			the_content();
			endwhile; endif; wp_reset_query(); 
		?>
		
	</div>


</div>