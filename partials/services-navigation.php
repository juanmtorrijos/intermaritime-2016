<div 
	class="services-navigation"
	<?php if ( has_post_thumbnail() ) : ?>
		style="background-image: url('<?php echo the_post_thumbnail_url(); ?>')"
	<?php endif; ?>
>
	<div class="main-content">
		<div class="service-title">
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>    
				<h1><?php the_title(); ?></h1>
			<?php endwhile; else : ?>
				<h1>Services Title goes here...</h1>
			<?php endif; wp_reset_query(); ?>
		</div>
	</div>
</div>