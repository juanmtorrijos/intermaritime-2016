<div class="name-email">
	<div class="name">
		<label>
			<p>Company Name</p>
			[text company-name placeholder "Company Name"]
		</label>
	</div>
	<div class="name">
		<label>
			<p>Contact Name</p>
			[text your-name placeholder "Your Name"]
		</label>
	</div>
</div>
<div class="name-email">
	<div class="email">
		<label>
			<p>Your Email</p>
			[email* your-email placeholder "Your Email"]
		</label>
	</div>
	<div class="email">
		<label>
			<p>Phone Number</p>
			[tel your-tel placeholder "Contact Phone Number"]
		</label>
	</div>
</div>
<div class="service-select">
	<p>What Service are you Interested In:</p>
	[select service-select multiple "Class Services" "Statutory Surveys" "Legal Services" "Training Services" "Surveys Services" "Condition Surveys" "Insurance Services"]
</div>
<div class="message">
	<label>
		<p>Your Message</p>
		[textarea your-message placeholder "Tell us anything we would need to know here..."]
	</label>
</div>
[submit "Send"]