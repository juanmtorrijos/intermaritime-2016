<div class="home-banner">
	<div class="banner-slogan">
		<h1>Integral Maritime Solutions</h1>
		<h2>Certification · Legal · Training · Surveys · Insurance</h2>
		<a href="#0" class="play-video">
            <img src="<?php echo THEMEROOT; ?>/images/play_button.png" alt="Play Video Button">
        </a>
	</div>
</div>