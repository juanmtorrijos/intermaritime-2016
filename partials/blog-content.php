<div class="page-title">
	<div class="main-content">
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>    
			<h1><?php the_title(); ?></h1>
		<?php endwhile; else : ?>
			<h1>Title goes here...</h1>
		<?php endif; wp_reset_query(); ?>
	</div>
</div>

<div class="main-content">

	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

		<p><?php the_content(); ?></p>

	<?php endwhile; else : ?>

		<p>No posts...</p>

	<?php endif; wp_reset_query(); ?>

</div>