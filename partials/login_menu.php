<div class="login-button">
	<?php 
	
	    $top_bar_args = array(
	        'theme_location'    => 'top_bar_menu',
	        'container'         => '',
	        'menu_class'        => 'top-bar-nav',
	        'echo'              => true,
	        'fallback_cb'       => 'wp_page_menu',
	    );

	    wp_nav_menu( $top_bar_args );

	 ?>
</div>