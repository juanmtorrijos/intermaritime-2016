<div class="corporate-video">
	<div class="the-video">
		
		<video id="intermaritime-corporate-video"

			controls 
			preload="auto"
			poster="<?php echo home_url(); ?>/wp-content/uploads/2016/01/santiago-torrijos-video-intermaritime-video-poster.jpg"
	      	data-setup='{ "aspectRatio":"960:540" }'>

	    	<source src="//s3.amazonaws.com/intermaritime-website/intermaritime-video.mp4" type='video/mp4' />

	    	<source src="//s3.amazonaws.com/intermaritime-website/intermaritime-video.webmhd.webm" type='video/webm' />

	  	</video>
		
	</div>
</div>