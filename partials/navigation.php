<?php    
    /**
    * Displays a navigation menu
    * @param array $args Arguments
    */
    $args = array(
        'theme_location'    => 'main_menu',
        'container'         => '',
        'container_class'   => 'main-nav',
        'menu_class'        => 'main-nav',
        'echo'              => true,
        'fallback_cb'       => 'wp_page_menu',
    );

    wp_nav_menu( $args );

?>