<div class="work-with-us">

  <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

    <?php the_content(); ?>

  <?php endwhile; endif; ?>

  <?php echo do_shortcode('[contact-form-7 id="6" title="Contact form 1"]'); ?>

</div>