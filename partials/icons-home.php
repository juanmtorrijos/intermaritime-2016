<div class="home-icons">

	<?php 

	$services_list = array(
		'Class' => 'icsclass.org',
		'Legal' => 'intermaritimelegal.org',
		'Training' => 'panamamaritimetraining.com',
		'Surveys' => 'icsurveys.org',
		'Insurance' => 'aqmltd.com',
	);

	foreach ( $services_list as $department => $link) :

	?>

		<div class="int-services">

			<a href="<?php echo 'http://' . $link;  ?>" target="_blank">

				<img src="<?php echo THEMEROOT; ?>/images/intermaritime-<?php echo strtolower($department); ?>.png" alt="Class Services">

				<p><?php echo $department; ?></p>
				
			</a>

		</div>

	<?php endforeach; ?>
	
</div>