<div class="contact-modal">
	<div class="modal-form">
		<h3>Contact Form</h3>
		<p>Fill this form and we will contact you in the next hours.</p>
		<?php echo do_shortcode( '[contact-form-7 id="6" title="Contact form 1"]' ); ?>
	</div>
</div>