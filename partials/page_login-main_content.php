<div class="main-content">

	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

		<p><?php the_content(); ?></p>

	<?php endwhile; else : ?>

		<p>No posts...</p>

	<?php endif; wp_reset_query(); ?>

	<?php if ( is_page( 'login-area' ) ) : ?>

	<div class="ics-forms">
		
		<?php 

			$ics_forms_args = array(
				'post_type' => 'ics-forms-info',
				'post_parent' => 0
			);

			$ics_forms = new WP_Query( $ics_forms_args );

			if ( $ics_forms->have_posts() ) : 

		?>

		<ul>

		<?php while ( $ics_forms->have_posts() ) : $ics_forms->the_post(); ?>
			

			<li>
				<a href="<?php the_permalink(); ?>">
					<?php the_title(); ?>
				</a>
			</li>

			<?php endwhile; else : ?>

			<li>No Forms Yet...</li>

			<?php endif; wp_reset_query(); ?>

		</ul>

	</div>

	<?php endif; ?>

</div>