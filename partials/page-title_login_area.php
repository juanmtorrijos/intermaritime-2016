<div class="page-title">

    <div class="page-title-content">
        
        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

            <h2 class="login-page-title">

            <?php if( !is_single() ) { 
                    the_title();
                } else {
                    echo 'ICS Forms and Information';
                } ?>

            </h2>
        <?php endwhile; endif; wp_reset_query();
 
            $top_bar_args = array(
                'theme_location'    => 'login_area_menu',
                'container'         => '',
                'menu_class'        => 'login-area-menu',
                'echo'              => true,
                'fallback_cb'       => 'wp_page_menu',
            );

            wp_nav_menu( $top_bar_args );

        ?>

    </div>
    
</div>