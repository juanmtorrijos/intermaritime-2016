<div class="work-with-us">
	<h2>Ready To Work With Us?</h2>
	<p>
		Contact us today and  a representative will help you very soon.<br>
		We will connect you with any of our partners worldwide to give you a fast response.
	</p>
	<?php if ( !is_page_template('contact_us-page.php') ) : ?>
		<a href="<?php echo home_url('contact-us'); ?>" class="contact-us">Contact Us</a>
	<?php endif; ?>
</div>