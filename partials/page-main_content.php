<div class="main-content">

	<div class="inner-container">

		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

			<p><?php the_content(); ?></p>

		<?php endwhile; else : ?>

			<p>No posts...</p>

		<?php endif; wp_reset_query(); ?>

	</div>

	<?php get_sidebar(); ?>

</div>