<div class="about-stats">
	<div class="about-text-stats">
		<div class="about-text">
			<p>
				"I would like to express profound thanks to the <span class="intermaritime-letters">InterMaritime</span> staff for highly efficient work. I am confident that every member of the staff taking an active part in the work will make a valuable contribution to strengthening the position of <span class="intermaritime-letters">InterMaritime.</span>"
			</p>
			<p class="short">
				<span class="intermaritime-letters">Santiago Torrijos</span><br>
				<span class="intermaritime-letters">Chairman</span>
			</p>
		</div>

		<div class="stats">
			<div class="vesels">
				<p>
					<span class="number" id="vessels">70</span>
					<span class="text">Vessels Covered</span>
				</p>
			</div>

			<div class="countries">
				<p>
					<span class="number" id="countries">3</span>
					<span class="text">Countries Served</span>
				</p>
			</div>
		</div>
	</div>
</div>