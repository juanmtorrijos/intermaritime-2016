<div class="main-content">
    <div class="kwes-form" v-cloak>
      <form method="post" action="https://kwes.io/api/foreign/forms/VIi6BMyEWH5DyQ2lqU90" redirect="/contact-us/thank-you">
        <label for="name">Your Name <span class="required">(required)</span></label>
          <input type="text" name="name" rules="required|max:255" placeholder="John Smith Jr." required>
        <label for="email">Your Email <span class="required">(required)</span></label>
          <input type="email" name="email" rules="email" placeholder="your_name@mail.com" required>
        <label for="phone">Contact Phone</label>
          <input type="text" name="phone" rules="numeric" placeholder="+1-777-888-9999">
        <label for="company_name">Company Name</label>
          <input type="text" name="company_name" rules="max:255" placeholder="Maritime Company Inc.">
        <label for="services">Services Required</label>
        <div class="kw-checkbox-group" rules="required">
            <label class="container">
              <input type="checkbox" name="services" value="Vessel Certification (Class & Statutory)" label="Vessel Certification (Class & Statutory)">
            </label>
            <label class="container">
              <input type="checkbox" name="services" value="Maritime Legal Services" label="Maritime Legal Services">
            </label>
            <label class="container">
              <input type="checkbox" name="services" value="Training and Licencing Services" label="Training and Licencing Services">
            </label>
            <label class="container">
              <input type="checkbox" name="services" value="Condition Survey Services" label="Condition Survey Services">
            </label>
            <label class="container">
              <input type="checkbox" name="services" value="Bunker Survey Services" label="Bunker Survey Services">
            </label>
            <label class="container">
              <input type="checkbox" name="services" value="Maritime Insurance" label="Maritime Insurance">
            </label>
            <label class="container">
              <input type="checkbox" name="services" value="Other" label="Other">
              <input type="text" name="other_services" class="other_services" placeholder="Specify">
            </label>
        </div>
        <label for="message">Your Message</label>
        <textarea name="message"></textarea>
        <button type="submit">Submit</button>
      </form>
    </div>
  </div>