<?php
define ( 'THEMEROOT', get_template_directory_uri() );
define ( 'IMAGESPATH', THEMEROOT . '/images' );

require get_template_directory() . '/includes/register_my_styles_and_scripts.php';
require get_template_directory() . '/includes/set-featured-image.php';
require get_template_directory() . '/includes/intermaritime-name.php';
require get_template_directory() . '/includes/google-analytics-tracking.php';
require get_template_directory() . '/includes/wp-title-hack-for-home.php';
require get_template_directory() . '/includes/new_excerpt_more.php';
require get_template_directory() . '/includes/meta-viewport.php';
require get_template_directory() . '/includes/theme-favicon.php';
require get_template_directory() . '/includes/google-fonts.php';
require get_template_directory() . '/includes/contact-form-7-customization.php';
require get_template_directory() . '/includes/add_html5_support.php';
require get_template_directory() . '/includes/html5shimie9.php';
require get_template_directory() . '/includes/js-class-body.php';
require get_template_directory() . '/includes/post-thumb.php';
require get_template_directory() . '/includes/fontawesome.php';
require get_template_directory() . '/includes/register-sidebars.php';
require get_template_directory() . '/includes/shortcodes.php';
require get_template_directory() . '/includes/remove-website-url.php';
require get_template_directory() . '/includes/register-menus.php';
require get_template_directory() . '/includes/custom_wordpress_from_name.php';
require get_template_directory() . '/includes/intermaritime_loginout_menu_link.php';
require get_template_directory() . '/includes/ajax-login.php';
require get_template_directory() . '/includes/new_user_capabilities_form_tag.php';
require get_template_directory() . '/includes/vessel_registration_calculator.php';

/**
 * Customizing Login Form
 * https://codex.wordpress.org/Customizing_the_Login_Form
 */
require get_template_directory() . '/includes/customizing_login_form.php';